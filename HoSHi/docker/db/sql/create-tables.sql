DROP TABLE IF EXISTS `patients`;

create table IF not exists `patients`
	(
		`userId`               INT(20) AUTO_INCREMENT,
		`name`             	VARCHAR(20) NOT NULL,
		`sex`					INT(2) DEFAULT NULL,
		`birthday`     		Datetime DEFAULT NULL,
		`hp_name`            	VARCHAR(20) NOT NULL,
		`hp_code`           	INT(8) DEFAULT NULL,
		`updated_at`       Datetime DEFAULT NULL,
		PRIMARY KEY (`userId`)
	) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

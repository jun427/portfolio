# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config['JSON_AS_ASCII'] = False
app.config["JSON_SORT_KEYS"] = False

@app.route("/", methods=['GET'])
def index():
	return render_template("QRreader.html")

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5005, debug=True)
